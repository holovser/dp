package cvut.fit.holovser.controllers.interceptors

import cvut.fit.holovser.constants.zone
import cvut.fit.holovser.model.els.User
import cvut.fit.holovser.repositories.els.UserRepository
import org.springframework.web.servlet.HandlerInterceptor
import java.time.LocalDateTime
import java.time.ZoneOffset
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Interceptor which checks if AccessToken header value corresponds with any existing user and
 * if the tokenTimeToLive parameter is represents a time in the future
 *
 * @property userRepository UserRepository
 * @constructor
 */
class AuthInterceptor(var userRepository: UserRepository): HandlerInterceptor {

    override fun preHandle(request: HttpServletRequest,
                           response: HttpServletResponse,
                           handler: Any): Boolean {
        if ( request.method == "OPTIONS" ) {
            return true
        }

        val accessToken: String? = request.getHeader("AccessToken")
        var user: User? = accessToken?.let { userRepository.findByAuth_AccessToken(it) }


        if (accessToken == null || user == null) {
            response.status = 403
            return false
        } else {
            if (LocalDateTime.now(ZoneOffset.UTC)
                    .isAfter(
                        user.auth?.tokenTimeToLive?.let {
                            LocalDateTime
                                .ofEpochSecond(it, 0, zone)
                        })) {
                response.status = 403
                return false
            }
        }
        return true
    }

}