package cvut.fit.holovser.util

import org.elasticsearch.common.xcontent.XContentBuilder

/**
 * Function which builds an els schema from a schemaObj argument
 *
 * @param schemaObj Map<String, Any>
 * @param builder XContentBuilder
 */
@Suppress("UNCHECKED_CAST")
fun elsSchemaBuilder(schemaObj: Map<String, Any>, builder: XContentBuilder) {
    for ((key, value) in schemaObj) {

        if (value is Map<*, *>) {
            builder.startObject(key)
                builder.startObject("properties")
                    elsSchemaBuilder(value as Map<String, Any>, builder)
                builder.endObject()
            builder.endObject()

        } else if (value is List<*>) {
            builder.startObject(key)
                builder.field("type", "nested")
                builder.startObject("properties")
                    elsSchemaBuilder(value[0] as Map<String, Any>, builder)
                builder.endObject()
            builder.endObject()

        } else if (value is String) {
            var type = when(value) {
                "searchable text" -> "text"
                "as-is text" -> "keyword"
                "integer" -> "integer"
                "float" -> "double"
                "boolean" -> "boolean"
                else -> "text"
            }
            builder.startObject(key)
            builder.field("type", type)
            builder.endObject()
        }
    }
}