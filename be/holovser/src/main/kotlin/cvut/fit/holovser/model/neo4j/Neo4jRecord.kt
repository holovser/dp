package cvut.fit.holovser.model.neo4j

open class Neo4jRecord

class PrimitiveRecord(var value: Any) : Neo4jRecord()

class ComplexRecord: Neo4jRecord() {
    var labels: Iterable<String> = emptyList()
    var properties: Map<String, Any> = emptyMap()
}