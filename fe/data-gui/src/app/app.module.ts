import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {InitialSetupComponent} from './components/initial-setup/initial-setup.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ElasticConfigComponent} from './components/elastic-config/elastic-config.component';
import {Neo4jConfigComponent} from './components/neo4j-config/neo4j-config.component';
import {StoreModule} from "@ngrx/store";
import {configReducer} from "./state/reducers";
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {ElasticSchemaItemComponent} from './components/elastic-schema-item/elastic-schema-item.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {SignupComponent} from './components/signup/signup.component';
import {LoginComponent} from "./components/login/login.component";
import {InterceptService} from "./services/intercept/intercept.service";
import { SummaryComponent } from './components/summary/summary.component';
import { Neo4jSummaryComponent } from './components/neo4j-summary/neo4j-summary.component';
import { ElsSummaryComponent } from './components/els-summary/els-summary.component';


@NgModule({
  declarations: [
    AppComponent,
    InitialSetupComponent,
    LoginComponent,
    ElasticConfigComponent,
    Neo4jConfigComponent,
    ElasticSchemaItemComponent,
    SignupComponent,
    SummaryComponent,
    Neo4jSummaryComponent,
    ElsSummaryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    StoreModule.forRoot({dbConfigState: configReducer}),
    StoreDevtoolsModule.instrument({}),
    FormsModule,
    HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: InterceptService, multi: true  }
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
