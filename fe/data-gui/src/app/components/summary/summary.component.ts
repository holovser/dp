import { Component, OnInit } from '@angular/core';
import {State} from "../../state/reducers";
import {Store} from "@ngrx/store";

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {

  constructor(private store: Store<{ dbConfigState: State }>) { }

  state?: State

  ngOnInit(): void {
    this.store.subscribe((store: { dbConfigState: State }) => {
      this.state = store.dbConfigState;
    });
  }

}
