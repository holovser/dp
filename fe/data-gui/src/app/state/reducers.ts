import { createReducer, on } from '@ngrx/store';
import {
  authenticateUser,
  clearElsSchema,
  createNeo4jDb,
  logOutUser,
  updateAuthError,
  updateElsConfig,
  updateElsSchema, updateNeo4jAccessToken
} from "./actions";


export interface Neo4jDbInterface {
  accessToken?: string,
}

export interface AuthInterface {
  username?: string,
  accessToken?: string,
  refreshToken?: string,
  tokenTimeToLive?: number,
  authError?: string
}

export interface ElsDbInterface {
  index?: string,
  accessToken?: string,
  schema: object,
  schemaLocked: boolean
}

export interface State {
  els?: ElsDbInterface,
  neo4j?: Neo4jDbInterface,
  auth?: AuthInterface
}

export const initialState: State = {
  els: {
    schema: {},
    schemaLocked: false
  }
};

export const configReducer = createReducer(
  initialState,
  on(updateElsConfig, (state:State, elsObj: {els: ElsDbInterface}) => {
    return {
      ...state,
      els: {
        ...(elsObj.els),
        schema: elsObj.els.schema ? elsObj.els.schema : {}
      },
    };
  }),
  on(updateElsSchema, (state: State, newSchema: {schema: any}) => {
    return {
      ...state,
      els: {
        schemaLocked: false,
        ...state.els,
        schema: newSchema.schema ? newSchema.schema : state.els?.schema
      }
    }
  }),
  on(authenticateUser, (state: State, userData: {user: State}) => {
    return {
      ...state,
      auth: userData.user.auth,
      els: {
        schemaLocked: false,
        ...(userData.user.els),
        // @ts-ignore
        schema: userData.user.els?.schema ? JSON.parse(userData.user.els?.schema?.toString()) : {}
      },
      neo4j: userData.user.neo4j
    };
  }),
  on(updateAuthError, (state: State, error: {authError: string | undefined}) => {
    return {
      ...state,
      auth: {
        ...(state.auth),
        authError: error.authError
      }
    }
  }),
  on(logOutUser, (state: State) => {
    return initialState
  }),
  on(clearElsSchema, (state: State) => {
    return {
      ...state,
      els: {
        schemaLocked: false,
        ...state?.els,
        schema: {}
      }
    }
  }),
  on(createNeo4jDb, (state: State, neo4jData: {neo4j: Neo4jDbInterface}) => {
    return {
      ...state,
      neo4j: neo4jData.neo4j
    };
  }),
  on(updateNeo4jAccessToken, (state: State, token: { accessToken: string }) => {
    return {
      ...state,
      neo4j: {
        ...(state.neo4j!),
        accessToken: token.accessToken
      }
    };
  })
);
