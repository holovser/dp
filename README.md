# This is Master's Thesis written by Serhii Holovko
This folder contains text, source code of backend and frontend parts. There is also a folder with a backend documentation and a folder with a Postman collection.

## Backend

The backend is located in the /be folder. Its documentation can be found in /be_documentation

## Frontend

The frontend functionality is located in the /fe folder.


## Postman

The collection with prepared HTTP requests is located in the /postman folder.
There may be a necessity to change AccessToken header or some path or query variables in case you would work with a custom user. 
Keep in mind that a test user has a locked schema, for playing with schema on the frontend create another one.

## Application

The database platform GUI can be accesed from: https://diploma-fe.web.app/

## Contact

In case the application would not operate or there would be any kind of a problem, do not hesitate to write an email: holovser@gmail.com 
You can also call or write an SMS: +420773297974