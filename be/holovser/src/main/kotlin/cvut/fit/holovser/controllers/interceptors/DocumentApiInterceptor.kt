package cvut.fit.holovser.controllers.interceptors

import cvut.fit.holovser.model.els.User
import cvut.fit.holovser.repositories.els.UserRepository
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.HandlerMapping
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Interceptor which handles all requests sent to the ELS Api
 * Checks if a user with AccessToken header value exists and if it has any assigned index
 *
 * @property userRepository UserRepository
 * @constructor
 */
class DocumentApiInterceptor(var userRepository: UserRepository): HandlerInterceptor {

    override fun preHandle(request: HttpServletRequest,
                           response: HttpServletResponse,
                           handler: Any): Boolean {

        val accessToken: String? = request.getHeader("AccessToken")

        if (accessToken == null) {
            response.status = 403
            return false
        }

        val user: User? = userRepository.findByEls_AccessToken(accessToken)
        val pathVariables = request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE) as Map<*, *>
        val index = pathVariables["index"] as String?

        if (user?.els?.index != index) {
            response.status = 403
            return false
        }

        return true
    }
    
}