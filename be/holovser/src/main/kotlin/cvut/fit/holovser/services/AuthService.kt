package cvut.fit.holovser.services

import cvut.fit.holovser.constants.zone
import cvut.fit.holovser.exceptions.AlreadyExistingUserException
import cvut.fit.holovser.exceptions.NoSuchUserException
import cvut.fit.holovser.exceptions.WrongCredentialsUserException
import cvut.fit.holovser.model.els.User
import cvut.fit.holovser.repositories.els.UserRepository
import org.springframework.stereotype.Component
import java.security.SecureRandom
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.*


@Component
class AuthService(var userRepository: UserRepository) {

    private val secureRandom: SecureRandom = SecureRandom()
    private val base64Encoder: Base64.Encoder = Base64.getUrlEncoder()
    private val tokenMinutesDuration: Long = 10


    /**
     * Create a new user in els in case there is no stored user with the same username
     *
     * @param username String
     * @param password String
     * @return User
     */
    fun createNewUser(username: String, password: String): User {
        if (userRepository.findByAuth_Username(username) == null) {
            val user = User();
            user.auth?.username = username
            user.password = password
            refreshUser(user)

            userRepository.save(user)


            return user
        } else {
            throw AlreadyExistingUserException("User with such username already exists")
        }
    }

    /**
     * check username, password combintaion and return a user in case of a success,
     * otherwise throw WrongCredentialsUserException
     *
     * @param username String
     * @param password String
     * @return User
     */
    fun loginUser(username: String, password: String): User {
        val user: User? = userRepository.findByAuth_Username(username)

        if (password == user?.password) {
            refreshUser(user)
            userRepository.save(user)
        } else {
            throw WrongCredentialsUserException("$username password does not match")
        }

        return user
    }


    /**
     * Retrieving user from els by its accessToken
     *
     * @param accessToken String
     * @return User
     */
    fun getUserByAccessToken(accessToken: String): User {
        val user: User? = userRepository.findByAuth_AccessToken(accessToken)

        if (user == null) {
            throw NoSuchUserException("No user with such accessToken found");
        } else {
            return user
        }
    }


    /**
     * Generating new accessToken, refreshToken and assigning new tokenTimeToLive property
     * @param user User
     */
    private fun refreshUser(user: User) {
        user.auth?.accessToken = generateToken()
        user.auth?.tokenTimeToLive =
            LocalDateTime.now(ZoneOffset.UTC).plusMinutes(tokenMinutesDuration).toEpochSecond(zone);
        user.auth?.refreshToken = generateToken()
    }


    /**
     *
     * Method for creating new accessToken and refreshToken; The function assign new tokens to a user,
     * store a user in els and updates tokenTimeToLive property
     *
     * @param accessToken String
     * @param refreshToken String
     * @return User
     */
    fun refreshToken(accessToken: String, refreshToken: String): User {
        var user: User? = userRepository.findByAuth_AccessToken(accessToken)

        if ( user?.auth?.refreshToken == refreshToken) {
            refreshUser(user)
            userRepository.save(user)

            return user
        } else {
            throw WrongCredentialsUserException("RefreshToken is not valid")
        }
    }


    /**
     * Generate 24 random bytes which are then transformed into a string
     * @return String
     */
    fun generateToken(): String {
        val randomBytes = ByteArray(24)
        secureRandom.nextBytes(randomBytes)
        return base64Encoder.encodeToString(randomBytes)
    }


    /**
     * Search for a user by its neo4j accessToken, if user is not found throw NoSuchUserException
     *
     * @param accessToken String
     * @return User
     */
    fun authenticateUserByNeo4jToken(accessToken: String): User {
        return userRepository.findByNeo4j_AccessToken(accessToken)
            ?: throw NoSuchUserException("User with such neo4j accessToken not found")
    }
}