import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {HttpService} from "../../services/auth/http.service";

@Component({
  selector: 'app-initial-setup',
  templateUrl: './initial-setup.component.html',
  styleUrls: ['./initial-setup.component.scss']
})
export class InitialSetupComponent implements OnInit {

  public submitted?: boolean = false;

  initSetupForm = new FormGroup({
    dbTypeForm: new FormGroup({
      dbType: new FormControl('non-connected'),
    })
  });

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private httpService: HttpService
              ) {
  }

  ngOnInit(): void {
    this.activatedRoute.queryParamMap.subscribe((values: ParamMap) => {
      this.submitted = JSON.parse(<string>values.get('submitted'));
    });

    this.httpService.fetchElsConfig();
  }

  onSubmit() {
    this.submitted = true;
    this.router.navigate(['/init-setup'], {queryParams: {submitted: true}});
  }

  onReset() {
    this.initSetupForm.reset();
    this.submitted = false;
  }

}
