package cvut.fit.holovser.exceptions

class AlreadyExistingUserException(message: String): Exception(message)

class WrongCredentialsUserException(message: String): Exception(message)

class NoSuchUserException(message: String): Exception(message)

class ForbiddenIndexNameException(message: String): Exception(message)