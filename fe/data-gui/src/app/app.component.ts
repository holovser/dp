import {Component, OnInit} from '@angular/core';
import {AuthInterface, ElsDbInterface, State} from "./state/reducers";
import {Store} from "@ngrx/store";
import {logOutUser} from "./state/actions";
import {Router} from "@angular/router";
import {HttpService} from "./services/auth/http.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private store: Store<{ dbConfigState: State }>,
              private router: Router, private httpService: HttpService) {
  }

  auth?: AuthInterface = undefined;
  els?: ElsDbInterface = undefined;

  ngOnInit(): void {
    this.store.subscribe((store: { dbConfigState: State }) => {
      this.auth = store.dbConfigState.auth;
      this.els = store.dbConfigState.els;
    });
  }


  logOut() {
    this.store.dispatch(logOutUser());
    this.httpService.stopRefreshing();
    this.router.navigate(['/']);
  }
}
