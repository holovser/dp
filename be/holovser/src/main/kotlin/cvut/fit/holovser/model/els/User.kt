package cvut.fit.holovser.model.els

import com.fasterxml.jackson.annotation.JsonIgnore
import cvut.fit.holovser.model.http.Neo4jRequest
import org.springframework.data.annotation.Id
import org.springframework.data.elasticsearch.annotations.DateFormat
import org.springframework.data.elasticsearch.annotations.Document
import org.springframework.data.elasticsearch.annotations.Field
import org.springframework.data.elasticsearch.annotations.FieldType
import java.time.LocalDateTime

@Document(indexName = "user")
class User {
    @Id
    lateinit var id: String;

    @JsonIgnore
    lateinit var password: String
    var auth: Auth? = Auth()
    var els: Els = Els()
    var neo4j: Neo4j? = null
}


class Auth {
    lateinit var accessToken: String
    lateinit var username: String

    @Field(type = FieldType.Long)
    var tokenTimeToLive: Long? = null
    lateinit var refreshToken: String
}

open class Neo4j {
    var accessToken: String = ""
}

class Els {
    var index: String? = null
    var accessToken: String? = null
    var schemaLocked: Boolean = false
    var schema: String? = null
}