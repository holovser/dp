import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {AuthInterface, State} from "../../state/reducers";
import {Store} from "@ngrx/store";
import {Router} from "@angular/router";
import {HttpService} from "../auth/http.service";

@Injectable({
  providedIn: 'root'
})
export class ElsService {

  constructor(private store: Store<{dbConfigState: State}>,
              private httpService: HttpService,
              private router: Router) { }

  private auth: AuthInterface | undefined;

  ngOnInit(): void {
    this.store.subscribe((store: {dbConfigState: State}) => {
      this.auth = store['dbConfigState']['auth'];
    });
  }
}
