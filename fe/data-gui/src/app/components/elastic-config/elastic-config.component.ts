import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {ElsDbInterface, State} from "../../state/reducers";
import {clearElsSchema} from "../../state/actions";
import {HttpService} from "../../services/auth/http.service";

@Component({
  selector: 'app-elastic-config',
  templateUrl: './elastic-config.component.html',
  styleUrls: ['./elastic-config.component.scss']
})
export class ElasticConfigComponent implements OnInit {

  public els?: ElsDbInterface;
  public elsSchema = {}
  public schemaSubmit: boolean = false

  constructor(private store: Store<{dbConfigState: State}>,
              private httpService: HttpService) { }

  ngOnInit(): void {
    this.store.subscribe((store: {dbConfigState: State}) => {
      this.els = store.dbConfigState.els;
      this.elsSchema = JSON.parse(JSON.stringify((store.dbConfigState.els?.schema)));
    });
  }

  onSchemaSubmit() {
    this.httpService.createElasticIndex(this.elsSchema!, this.els);
    this.schemaSubmit = true;
  }

  onReset() {
    this.store.dispatch(clearElsSchema());
    this.schemaSubmit = false
    this.httpService.createElasticIndex({}, this.els);
  }

  public isEmpty(item: any): boolean {
    return Object.keys(item).length === 0
  }

}
