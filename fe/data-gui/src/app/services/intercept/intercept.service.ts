import { Injectable } from '@angular/core';
import {Router} from "@angular/router";
import {HttpEvent, HttpHandler, HttpRequest} from "@angular/common/http";
import {catchError, Observable, throwError} from "rxjs";
import {AuthInterface, State} from "../../state/reducers";
import {Store} from "@ngrx/store";

@Injectable({
  providedIn: 'root'
})
export class InterceptService {

  constructor(public router: Router,
              private store: Store<{ dbConfigState: State }>) {
    this.store.subscribe((store: { dbConfigState: State }) => {
      this.auth = store.dbConfigState.auth;
    });
  }

  auth?: AuthInterface  = undefined;

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let modifiedReq = req.clone()
    if ( this.auth?.accessToken != undefined ) {
      modifiedReq = req.clone({
        headers: req.headers.set('AccessToken', `${this.auth?.accessToken}`)
      });
    }

    return next.handle(modifiedReq).pipe()
  }
}
