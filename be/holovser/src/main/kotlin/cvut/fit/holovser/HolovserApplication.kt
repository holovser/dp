package cvut.fit.holovser

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


/**
 * Main Spring application class which is responsible for starting an application and may contain
 * different configuration annotations
 */
@SpringBootApplication
class HolovserApplication


/**
 * A high order function which starts a Spring application
 *
 * @param args Array<String>
 */
fun main(args: Array<String>) {
	runApplication<HolovserApplication>(*args)
}



