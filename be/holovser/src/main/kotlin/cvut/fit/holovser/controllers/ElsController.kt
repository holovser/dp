package cvut.fit.holovser.controllers

import cvut.fit.holovser.exceptions.NoSuchUserException
import cvut.fit.holovser.model.http.UpdateIndexRequest
import cvut.fit.holovser.model.els.User
import cvut.fit.holovser.services.AuthService
import cvut.fit.holovser.services.ElsService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
class ElsController(var authService: AuthService,
                    var elsService: ElsService) {


    @PostMapping("/index")
    fun createIndex(@RequestHeader accessToken: String,
                    @RequestBody elsRequest: UpdateIndexRequest
    ): ResponseEntity<User> {
        try {
            val user: User = authService.getUserByAccessToken(accessToken)

            if (!user.els.schemaLocked) {
                elsService.createNewIndex(user, elsRequest.elsSchema)
            }
            return ResponseEntity.status(202).body(user)
        } catch (ex: NoSuchUserException) {
            return ResponseEntity.status(403).build()
        } catch (ex: Throwable) {
            println(ex.message)
            return ResponseEntity.internalServerError().build()
        }
    }

    @GetMapping("/index")
    fun fetchElsConfig(@RequestHeader accessToken: String): ResponseEntity<Any> {

        try {
            val user: User = authService.getUserByAccessToken(accessToken)

            return ResponseEntity.status(202).body(user.els)
        } catch (ex: NoSuchUserException) {
            return ResponseEntity.status(403).build()
        } catch (ex: Throwable) {
            println(ex.message)
            return ResponseEntity.internalServerError().build()
        }
    }
}