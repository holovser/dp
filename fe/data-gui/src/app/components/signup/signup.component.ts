import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpService} from "../../services/auth/http.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthInterface, State} from "../../state/reducers";
import {Store} from "@ngrx/store";
import {updateAuthError} from "../../state/actions";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, OnDestroy {

  constructor(private route:ActivatedRoute,
              private httpService: HttpService,
              private store: Store<{ dbConfigState: State }>) {}

  public profileForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  auth?: AuthInterface = undefined;

  ngOnInit(): void {
    this.store.subscribe((store: { dbConfigState: State }) => {
      this.auth = store.dbConfigState.auth;
    });
  }

  ngOnDestroy(): void {
    this.store.dispatch(updateAuthError({authError: undefined}));
  }


  private signup(username: string, password: string) {
    this.httpService.signup(username, password);
  }

  public onSubmit() {
    this.signup(this.profileForm.value['username'], this.profileForm.value['password']);
  }

}
