import {Component, Input, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {State} from "../../state/reducers";
import {UniqueKeyGeneratorService} from "../../services/id-generator/unique-key-generator.service";
import {updateElsSchema} from "../../state/actions";

@Component({
  selector: 'app-elastic-schema-item',
  templateUrl: './elastic-schema-item.component.html',
  styleUrls: ['./elastic-schema-item.component.scss']
})
export class ElasticSchemaItemComponent implements OnInit {

  @Input() item: any;
  @Input() parentItem: any;
  @Input() elsSchema?: object = {};

  public editableKey: string = '';
  public newKey: string = '';

  public dataTypes = [
    { id: 0, label: "object" },
    { id: 1, label: "searchable text" },
    { id: 2, label: "as-is text" },
    { id: 3, label: "integer" },
    { id: 4, label: "float"},
    { id: 5, label: "boolean" },
    { id: 6, label: "collection" }
  ];
  public chosenType: number = 0;

  public edited: boolean = false;
  public schemaLocked: boolean = false;

  public plusImagePath = "/assets/images/plus_icon.png";
  public removeImagePath = "/assets/images/remove_icon.png";

  constructor(private store : Store<{dbConfigState: State}>,
              private idGenerator:UniqueKeyGeneratorService) { }

  ngOnInit(): void {

    if ( this.isArray(this.item) ) {
      this.dataTypes = [this.dataTypes[0]]
    }

    let els = document.getElementsByClassName('keyParagraph');

    Array.prototype.forEach.call(els, (el) => {
      // @ts-ignore
      el.addEventListener('keypress', (evt) => {
        if (evt.which === 13) {
          evt.preventDefault();
          el.setAttribute('contenteditable', 'false');
          el.setAttribute('contenteditable', 'true');
        }
      });
    });

    this.store.subscribe((store: {dbConfigState: State}) => {
      this.schemaLocked = !!store.dbConfigState.els?.schemaLocked
    });

  }

  public updateElsSchema() {
    this.store.dispatch(updateElsSchema({schema: this.elsSchema!}));
  }

  public addAction() {

    // Angular bug of storing string in a number variable
    this.chosenType = parseInt(String(this.chosenType));

    let newInsertedValue: any = {};

    switch (this.chosenType) {
      case 0:
        newInsertedValue = {};
        break;
      case 6:
        newInsertedValue = [];
        break;
      default:
        newInsertedValue = this.dataTypes[this.chosenType].label;
    }

    if (this.isArray(this.item)) {
      this.item.push(newInsertedValue);
    } else if (this.isObject(this.item)) {
      this.item[this.idGenerator.getUniqueId()] = newInsertedValue;
    }
    this.updateElsSchema();
  }

  public isObject(item: any) {
    return typeof item === 'object' && !Array.isArray(item);
  }

  public isArray(item: any) {
    return Array.isArray(item);
  }

  public updateItem() {
    if ( this.newKey === '') {
      return;
    }
    this.item[this.newKey] = JSON.parse(JSON.stringify(this.item[this.editableKey]));

    delete this.item[this.editableKey];
    this.editableKey = this.newKey;
    this.newKey = '';
  }

  public buttonPressed(event: any, key: unknown) {
    // @ts-ignore
    this.editableKey = key;
    this.edited = true;

    this.newKey = event.target.innerHTML;

    //Enter pressed
    if ( event.which === 13 ) {
      this.updateItem();
      this.updateElsSchema();
    }
  }

  public stringify() {
    return JSON.stringify(this.item);
  }

  public isEmpty(item: any): boolean {
    return Object.keys(item).length === 0
  }

  public isPlusAllowed(item: any): boolean {
    if (this.isArray(item)) {
      // Array may contain only ony object which specifies the structure of this array
      return this.isEmpty(item);
    }

    return true;
  }

  public removeAction(key: any) {
    delete this.item[key];
  }
}



