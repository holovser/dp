package cvut.fit.holovser.controllers.api

import com.fasterxml.jackson.databind.ObjectMapper
import cvut.fit.holovser.model.els.User
import cvut.fit.holovser.model.http.ElsDocumentResponse
import cvut.fit.holovser.repositories.els.UserRepository
import cvut.fit.holovser.services.ElsService
import org.elasticsearch.action.index.IndexResponse
import org.elasticsearch.search.SearchHit
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/api/els")
class ElsApiController(var elsService: ElsService, var userRepository: UserRepository) {

    @PostMapping("/index/{index}")
    fun getRecords(@PathVariable index: String,
                   @RequestBody query: Map<String, Map<String, String>>): ResponseEntity<Any> {
        val result: Array<SearchHit>

        try {
            result = elsService.getRecords(query, index)
        } catch (ex: Throwable) {
            println(ex.message)
            return ResponseEntity.internalServerError().build()
        }

        val docs: List<ElsDocumentResponse> = result.map { hit ->
            ElsDocumentResponse(hit.id, hit.sourceAsMap)
        }

        return ResponseEntity.ok(docs)
    }


    @PutMapping("/index/{index}/doc/{docId}")
    fun updateRecord(@PathVariable index: String, @PathVariable docId: String,
                     @RequestParam fieldName: String, @RequestParam fieldValue: String
    ): ResponseEntity<Any> {

        elsService.updateDocument(index, docId, fieldName, fieldValue)

        return ResponseEntity.status(204).build()
    }

    @PostMapping("/index/{index}/doc")
    fun createNewDocument(@PathVariable index: String,
                          @RequestBody document: Map<String, Any>, @RequestHeader accessToken: String): ResponseEntity<Any> {
        val indexResponse: IndexResponse?

        try {
            val user: User = userRepository.findByEls_AccessToken(accessToken)
                ?: return ResponseEntity.status(404).build()

            indexResponse =
                 elsService.createDocument(index, ObjectMapper().writeValueAsString(document), user)
         } catch (ex: Throwable) {
             return ResponseEntity.internalServerError().build()
         }
        return ResponseEntity.status(201).body(ElsDocumentResponse(indexResponse.id))
    }

    @DeleteMapping("/index/{index}/doc/{docId}")
    fun deleteDocument(@PathVariable index: String, @PathVariable docId: String): ResponseEntity<Any> {
        elsService.deleteDocument(index, docId)

        return ResponseEntity.status(204).build()
    }
}