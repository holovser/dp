import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {AuthInterface, Neo4jDbInterface, State} from "../../state/reducers";
import {environment} from "../../../environments/environment";
import {HttpService} from "../../services/auth/http.service";

@Component({
  selector: 'app-neo4j-config',
  templateUrl: './neo4j-config.component.html',
  styleUrls: ['./neo4j-config.component.scss']
})
export class Neo4jConfigComponent implements OnInit {

  constructor(public store: Store<{dbConfigState: State}>,
              private httpService: HttpService) { }

  neo4jDb?: Neo4jDbInterface = undefined
  auth?: AuthInterface = undefined

  ngOnInit(): void {
    this.store.subscribe( (store: {dbConfigState: State}) => {
      this.auth = store.dbConfigState.auth
      this.neo4jDb = store.dbConfigState.neo4j
    });
  }

  onGraphDbCreate() {
    const baseUrl = environment.serverUrl + `/api/neo4j`;
    this.httpService.createNeo4jDb();
  }

}
