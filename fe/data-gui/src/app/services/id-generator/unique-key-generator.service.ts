import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UniqueKeyGeneratorService {

  public id: number = 1;
  constructor() {}

  public getUniqueId(): string {
    return String(this.id++);
  }
}
