package cvut.fit.holovser.controllers.api

import cvut.fit.holovser.exceptions.NoSuchUserException
import cvut.fit.holovser.model.els.User
import cvut.fit.holovser.model.http.Neo4jQuery
import cvut.fit.holovser.model.neo4j.Neo4jResult
import cvut.fit.holovser.services.AuthService
import cvut.fit.holovser.services.Neo4jService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/api/neo4j")
class Neo4jApiController(var neo4jService: Neo4jService,
                         var authService: AuthService) {

    @PostMapping
    fun match(@RequestBody query: Neo4jQuery,
              @RequestHeader accessToken: String): ResponseEntity<Neo4jResult> {
        val queryResult: Neo4jResult?

        try {
            val user: User = authService.authenticateUserByNeo4jToken(accessToken)
            queryResult = neo4jService.match(query, user)
        } catch (ex: NoSuchUserException) {
            return ResponseEntity.status(403).build()
        }
        catch (ex: Throwable) {
            print(ex)
            return ResponseEntity.status(400).build()
        }

        return ResponseEntity.ok(queryResult)
    }


    @PutMapping
    fun updateGraph(@RequestBody query: Neo4jQuery,
                    @RequestHeader accessToken: String): ResponseEntity<Any> {
        try {
            val user: User = authService.authenticateUserByNeo4jToken(accessToken)
            neo4jService.update(query, user)
        } catch (ex: NoSuchUserException) {
            return ResponseEntity.status(403).build()
        }
        catch (ex: Throwable) {
            print(ex)
            return ResponseEntity.status(400).build()
        }

        return ResponseEntity.ok().build()
    }


    @PostMapping("/record")
    fun createRecords(@RequestBody query: Neo4jQuery,
                      @RequestHeader accessToken: String): ResponseEntity<Any>{
        try {
            val user: User = authService.authenticateUserByNeo4jToken(accessToken)
            neo4jService.createRecords(query, user)
        } catch (ex: NoSuchUserException) {
            return ResponseEntity.status(403).build()
        }
        catch (ex: Throwable) {
            return ResponseEntity.status(400).build()
        }
        return ResponseEntity.status(201).build()
    }

    @DeleteMapping
    fun deleteRecords(@RequestBody query: Neo4jQuery,
                      @RequestHeader accessToken: String): ResponseEntity<Any>
    {
        try {
            val user: User = authService.authenticateUserByNeo4jToken(accessToken)
            neo4jService.match(query, user)
        } catch (ex: NoSuchUserException) {
            return ResponseEntity.status(403).build()
        }
        catch (ex: Throwable) {
            return ResponseEntity.status(400).build()
        }
        return ResponseEntity.status(204).build()
    }
}