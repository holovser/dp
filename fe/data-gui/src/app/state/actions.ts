import {createAction, props} from '@ngrx/store';
import {ElsDbInterface, Neo4jDbInterface, State} from "./reducers";


export const updateElsConfig =
  createAction('[Config Component] ChangeElsConfig',  props<{els: ElsDbInterface}>());

export const updateElsSchema =
  createAction('[Config Component] UpdateElsSchema',  props<{schema: any}>());

export const authenticateUser =
  createAction('[Login/Signup Component] Authenticate User',  props<{user: State}>());

export const logOutUser =
  createAction('[App Component] Log out User');

export const updateAuthError =
  createAction('[Login/Signup Component] Error',  props<{authError: string | undefined}>());

export const clearElsSchema =
  createAction('[Config Component] ClearElsSchema');

export const createNeo4jDb =
  createAction('[Config Component] CreateNeo4jDb', props<{neo4j: Neo4jDbInterface}>());

export const updateNeo4jAccessToken =
  createAction('[Config Component] UpdateNeo4jToken', props<{accessToken: string}>());
