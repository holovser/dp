package cvut.fit.holovser.services

import cvut.fit.holovser.model.neo4j.ComplexRecord
import cvut.fit.holovser.model.neo4j.Neo4jResult
import cvut.fit.holovser.model.neo4j.PrimitiveRecord
import org.neo4j.driver.Driver
import org.neo4j.driver.Record
import org.neo4j.driver.Result
import org.neo4j.driver.internal.value.NodeValue
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component


@Component
class QueryRunnerService {

    @Autowired
    lateinit var driver: Driver

    /**
     * Method which receives a query and executes it without transformation
     *
     * @param query String
     * @return Neo4jResult an array of Nodes
     */
    fun executeMatchQuery(query: String): Neo4jResult {

        var neo4jResult = Neo4jResult()

        driver.session().use { session ->
            var result: Result = session.run(query)
            result.list().forEach { it: Record ->
                it.values().forEach { value ->
                    when (value) {
                        is NodeValue -> {
                            val complexRecord = ComplexRecord();
                            complexRecord.labels = value.asNode().labels()
                            complexRecord.properties = value.asMap()
                            neo4jResult.records.add(complexRecord)
                        }
                        else -> neo4jResult.records.add(PrimitiveRecord(value.asObject()))
                    }
                }
            }
        }

        return neo4jResult
    }


    /**
     * Method which receives a query, executes it and does not transform/return any result
     * @param query String
     */
    fun executeCreateQuery(query: String) {
        driver.session().use { session ->
            session.run(query)
        }
    }
}