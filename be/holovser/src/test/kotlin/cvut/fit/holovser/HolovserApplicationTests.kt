package cvut.fit.holovser

import cvut.fit.holovser.util.addAuthenticOwner
import cvut.fit.holovser.util.matchQueryTransform
import cvut.fit.holovser.util.removeOwnerProperty
import org.elasticsearch.client.indices.PutMappingRequest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class HolovserApplicationTests {


	@Test
	fun removeOwnerPropertyTest() {
		assertEquals(
			"(n:Person{})",
			removeOwnerProperty("(n:Person{owner: \"False owner\"})"),
		)

		assertEquals(
			"(n:Person{})",
			removeOwnerProperty("(n:Person{owner: 'False owner'})"),
		)

		assertEquals(
			"(n:Person{name: 'John'})",
			removeOwnerProperty("(n:Person{owner: \"False owner\", name: 'John'})"),
		)

		assertEquals(
			"(n:Person{name: 'John'})",
			removeOwnerProperty("(n:Person{name: 'John'})"),
		)
	}

	@Test
	fun addAuthenticOwnerTest() {

		assertEquals(
			"MATCH (n:PERSON{, owner: 'holovser' })",
			addAuthenticOwner("MATCH (n:PERSON{})", "holovser")
		)

		assertEquals(
			"MATCH [rel:ACTED_IN{, owner: 'holovser' }]",
			addAuthenticOwner("MATCH [rel:ACTED_IN{}]", "holovser")
		)


		assertEquals(
			"MATCH (n:PERSON{ owner: 'holovser' })",
			addAuthenticOwner("MATCH (n:PERSON)", "holovser")
		)

		assertEquals(
			"MATCH [rel:ACTED_IN{ owner: 'holovser' }]",
			addAuthenticOwner("MATCH [rel:ACTED_IN]", "holovser")
		)


		assertEquals(
			"(n:PERSON{ name: 'Emil', owner: 'holovser' })",
			addAuthenticOwner("(n:PERSON{ name: 'Emil'})", "holovser")
		)

		assertEquals(
			"[rel:ACTED_IN{ name: 'Emil', owner: 'holovser' }]",
			addAuthenticOwner("[rel:ACTED_IN{ name: 'Emil'}]", "holovser")
		)

	}

}
