package cvut.fit.holovser.controllers

import cvut.fit.holovser.exceptions.AlreadyExistingUserException
import cvut.fit.holovser.model.http.UserRequest
import cvut.fit.holovser.model.els.User
import cvut.fit.holovser.services.AuthService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
class AuthController(var authService: AuthService) {

    @PostMapping("/user")
    fun registerUser(@RequestBody request: UserRequest): ResponseEntity<Any> {
        try {
            val user: User = authService.createNewUser(request.username, request.password)
            return ResponseEntity.status(201).body(user)
        }
        catch (ex: AlreadyExistingUserException) {
            println(ex.message)
            return ResponseEntity.status(409).build()
        }
        catch (ex: Throwable) {
            println(ex.message)
            return ResponseEntity.status(500).build()
        }
    }

    @PostMapping("/session")
    fun loginUser(@RequestBody userRequest: UserRequest): ResponseEntity<Any> {
        try {
            val user: User = authService.loginUser(userRequest.username, userRequest.password)
            return ResponseEntity.status(202).body(user)
        } catch (ex: Throwable) {
            println(ex.message)
            return ResponseEntity.status(403).build()
        }
    }

    @PutMapping("/token")
    fun refreshToken(@RequestHeader accessToken: String,
                     @RequestHeader refreshToken: String): ResponseEntity<Any> {
        try {
            val user: User = authService
                .refreshToken(refreshToken=refreshToken, accessToken=accessToken)
            return ResponseEntity.status(202).body(user)
        } catch (ex: Throwable) {
            println(ex.message);
            return ResponseEntity.status(403).build()
        }
    }
}