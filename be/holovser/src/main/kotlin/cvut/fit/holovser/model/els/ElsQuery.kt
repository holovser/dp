package cvut.fit.holovser.model.els


abstract class QueryValue

class StringQueryValue: QueryValue() {
    lateinit var value: String
    lateinit var type: TextSearchType
}

class NumberQueryValue: QueryValue() {
    var gt: String? = null
    var lt: String? = null
}

enum class TextSearchType() {
    MATCH,
    TERM
}