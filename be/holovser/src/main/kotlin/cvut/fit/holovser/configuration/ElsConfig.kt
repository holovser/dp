package cvut.fit.holovser.configuration

import org.elasticsearch.client.RestHighLevelClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.elasticsearch.client.ClientConfiguration
import org.springframework.data.elasticsearch.client.RestClients
import org.springframework.data.elasticsearch.core.ElasticsearchOperations
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories


@Configuration
@EnableElasticsearchRepositories(basePackages = ["cvut.fit.holovser.repositories.els"])
@ComponentScan(basePackages = ["cvut.fit.holovser"])
class ElsConfig {
    @Bean
    fun client(): RestHighLevelClient? {
        val clientConfiguration = ClientConfiguration.builder()
            .connectedTo("elastic-holovko-61e7c0.es.us-central1.gcp.cloud.es.io:443")
            .usingSsl()
            .withBasicAuth("elastic", "wz9BeHK2oLQcWX34FNNuuwfc")
            .withConnectTimeout(3000)
            .build()
        return RestClients.create(clientConfiguration).rest()
    }

    @Bean
    fun elasticsearchTemplate(): ElasticsearchOperations? {
        return ElasticsearchRestTemplate(client()!!)
    }

}