package cvut.fit.holovser.constants

import java.time.ZoneOffset


val reservedIndexNames = listOf("user", "kibana")
val zone: ZoneOffset = ZoneOffset.UTC