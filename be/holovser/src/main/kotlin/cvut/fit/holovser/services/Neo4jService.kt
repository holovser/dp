package cvut.fit.holovser.services

import cvut.fit.holovser.model.els.Neo4j
import cvut.fit.holovser.model.els.User
import cvut.fit.holovser.model.http.Neo4jQuery
import cvut.fit.holovser.model.neo4j.Neo4jResult
import cvut.fit.holovser.repositories.els.UserRepository
import cvut.fit.holovser.util.*
import org.springframework.stereotype.Component



@Component
class Neo4jService(
    var userRepository: UserRepository,
    var authService: AuthService, var queryRunnerService: QueryRunnerService
) {

    /**
     * Update a user record in els with neo4j graph data
     *
     * @param user User
     * @param neo4j Neo4j  graph data
     */
    fun createGraph(user: User) {
        user.neo4j = Neo4j()
        user.neo4j?.accessToken = authService.generateToken()
        userRepository.save(user)
    }

    /**
     *
     * Method which adds 'owner' property to a query, removes 'owner' property if a client specifies it.
     * After transformation a query is executed
     *
     * @param queryRequest Neo4jQuery
     * @param user User  User which performs a request
     * @return Neo4jResult
     */
    fun match(queryRequest: Neo4jQuery, user: User): Neo4jResult {
        val transformedQuery: String =
            matchQueryTransform(queryRequest.query, user.auth?.username!!)

        val escapedQuery: String =
            escapeSetCreateCommands(transformedQuery)

        return queryRunnerService.executeMatchQuery(escapedQuery)
    }


    /**
     *
     * Method which transforms and executes an update query
     *
     * @param queryRequest Neo4jQuery
     * @param user User Who performs a request
     */
    fun update(queryRequest: Neo4jQuery, user: User) {
        val transformedQuery: String =
            matchQueryTransform(queryRequest.query, user.auth?.username!!)

        var safeQuery: String = escapeDangerousCreateCommand(transformedQuery)
        safeQuery = escapeDangerousSetCommand(safeQuery)

        queryRunnerService.executeMatchQuery(safeQuery)
    }


    /**
     * Method which transforms, escapes all dangerous injections and execute a create query
     *
     * @param queryRequest Neo4jQuery
     * @param user User User which performs a request
     */
    fun createRecords(queryRequest: Neo4jQuery, user: User) {
        val transformedQuery: String =
            createQueryTransform(queryRequest.query, user.auth?.username!!)
        
        var safeQuery: String = escapeDangerousSetCommand(transformedQuery)
//        safeQuery = escapeDangerousCreateCommand(safeQuery)

        queryRunnerService.executeCreateQuery(safeQuery)
    }
}