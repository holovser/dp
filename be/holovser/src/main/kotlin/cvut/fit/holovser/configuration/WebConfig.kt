package cvut.fit.holovser.configuration

import cvut.fit.holovser.controllers.interceptors.AuthInterceptor
import cvut.fit.holovser.controllers.interceptors.DocumentApiInterceptor
import cvut.fit.holovser.repositories.els.UserRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@Configuration
@EnableWebMvc
class WebConfig(var userRepository: UserRepository): WebMvcConfigurer {
    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(AuthInterceptor(userRepository))
            .addPathPatterns("/index", "/graph")

        registry.addInterceptor(DocumentApiInterceptor(userRepository))
            .addPathPatterns("/api/els/**")

    }

    @Bean
    fun corsConfigurer(): WebMvcConfigurer? {
        return object : WebMvcConfigurer {
            override fun addCorsMappings(registry: CorsRegistry) {
                registry
                    .addMapping("/**")
                    .allowedMethods("PUT", "POST", "GET", "DELETE", "OPTIONS")
            }
        }
    }

}