package cvut.fit.holovser.services

import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.ObjectMapper
import cvut.fit.holovser.constants.reservedIndexNames
import cvut.fit.holovser.exceptions.ForbiddenIndexNameException
import cvut.fit.holovser.model.els.*
import cvut.fit.holovser.repositories.els.UserRepository
import cvut.fit.holovser.util.elsSchemaBuilder
import org.elasticsearch.action.delete.DeleteRequest
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.action.index.IndexResponse
import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.action.search.SearchType
import org.elasticsearch.action.update.UpdateRequest
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.client.indices.PutMappingRequest
import org.elasticsearch.common.xcontent.XContentFactory
import org.elasticsearch.common.xcontent.XContentType
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.script.Script
import org.elasticsearch.script.ScriptType
import org.elasticsearch.search.SearchHit
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.springframework.data.elasticsearch.core.ElasticsearchOperations
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates
import org.springframework.stereotype.Component


@Component
class ElsService(var elsTemplate: ElasticsearchOperations,
                 var userRepository: UserRepository,
                 var client: RestHighLevelClient,
                 var authService: AuthService) {

    /**
     *
     * Create a new index for a concrete user with a specified schema
     *
     * @param user User
     * @param schemaObj Map<String, Any>
     */
    fun createNewIndex(user: User, schemaObj: Map<String, Any>) {

        val newIndexName: String = user.auth?.username!!
        val isIndexNameForbidden =
            reservedIndexNames.firstOrNull { it.contains(newIndexName.lowercase())} != null

        if (!isIndexNameForbidden) {
//            If user already has an index -> delete it
            try {
                elsTemplate.indexOps(IndexCoordinates.of(newIndexName)).delete()
            } catch (_: Throwable) {}

            elsTemplate.indexOps(IndexCoordinates.of(newIndexName)).create()
            user.els.index = newIndexName
            user.els.schema = ObjectMapper().writeValueAsString(schemaObj)
            user.els.accessToken = authService.generateToken()

            updateElsSchema(user, schemaObj)
        } else {
            throw ForbiddenIndexNameException("Index with such name is forbidden")
        }
    }


    /**
     *
     * Method which creates and executes a match query specified by a query parameter
     *
     * @param query Map<String, Map<String, String>> custom query format
     * @param index String index name
     * @return Array<SearchHit>
     */
    fun getRecords(query: Map<String, Map<String, String>>, index: String): Array<SearchHit> {
        var boolQuery = QueryBuilders.boolQuery()

        val objectMapper = ObjectMapper()
        objectMapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)

        for ((key, value) in query) {
            var valueObj: QueryValue
            try {
                valueObj =
                    objectMapper.readValue(
                        objectMapper.writeValueAsString(value),
                        NumberQueryValue::class.java
                )
                boolQuery.must(
                    QueryBuilders
                        .rangeQuery(key).gt(valueObj?.gt).lt(valueObj?.lt)
                )
            } catch (ex: Throwable) {
                valueObj = (objectMapper.readValue(
                    objectMapper.writeValueAsString(value),
                    StringQueryValue::class.java
                ) as StringQueryValue)

                when(valueObj.type) {
                    TextSearchType.MATCH ->  boolQuery.must(QueryBuilders.matchQuery(key, valueObj.value))
                    TextSearchType.TERM -> boolQuery.must(QueryBuilders.termQuery(key, valueObj.value))
                }
//                boolQuery.must(QueryBuilders.rangeQuery(key))
            }
        }
        val builder: SearchSourceBuilder = SearchSourceBuilder().query(boolQuery)
        val searchRequest = SearchRequest(index)

        searchRequest.searchType(SearchType.DFS_QUERY_THEN_FETCH)
        searchRequest.source(builder)
        val response: SearchResponse = client.search(searchRequest, RequestOptions.DEFAULT)

        return response.hits.hits
    }

    /**
     *
     * Update a specific document's field with a fieldValue
     *
     * @param index String
     * @param docId String
     * @param fieldName String
     * @param fieldValue String
     */
    fun updateDocument(index: String, docId: String, fieldName: String, fieldValue: String) {
        val request = UpdateRequest(index, docId)
        val inline = Script(
            ScriptType.INLINE, "painless",
            "ctx._source.${fieldName} = '${fieldValue}'", emptyMap()
        )
        request.script(inline)
        client.update(request, RequestOptions.DEFAULT)
    }


    /**
     *
     * Create a specific document in 'index' index
     *
     * @param index String
     * @param documentJson String
     * @return IndexResponse
     */
    fun createDocument(index: String, documentJson: String, user: User): IndexResponse {
        val request = IndexRequest(index)
        request.source(documentJson, XContentType.JSON)
        val indexResponse: IndexResponse = client.index(request, RequestOptions.DEFAULT)

        user.els.schemaLocked = true

        return indexResponse
    }


    /**
     * Delete a document from index by its id
     *
     * @param index String
     * @param docId String
     */
    fun deleteDocument(index: String, docId: String) {
        val request = DeleteRequest(index, docId)
        client.delete(request, RequestOptions.DEFAULT)
    }


    /**
     * Updates Els schema
     *
     * @param user User
     * @param schemaObj Map<String, Any>
     */
    fun updateElsSchema(user: User, schemaObj: Map<String, Any>) {

        user.els.schema = ObjectMapper().writeValueAsString(schemaObj)

        val request = PutMappingRequest(user.els.index)
        val builder = XContentFactory.jsonBuilder()

        builder.startObject()
            builder.startObject("properties")
                elsSchemaBuilder(schemaObj, builder)
            builder.endObject()
        builder.endObject()

        request.source(builder)

        client.indices().putMapping(request, RequestOptions.DEFAULT)
        userRepository.save(user)
    }


}