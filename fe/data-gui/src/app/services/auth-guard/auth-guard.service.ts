import { Injectable } from '@angular/core';
import {CanActivate, Router} from "@angular/router";
import {HttpService} from "../auth/http.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(public auth: HttpService,
              public router: Router) { }

  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
