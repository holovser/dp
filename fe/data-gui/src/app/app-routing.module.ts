import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InitialSetupComponent} from "./components/initial-setup/initial-setup.component";
import {LoginComponent} from "./components/login/login.component";
import {loginPath, signUpPath} from "./constants/constants";
import {AuthGuardService} from "./services/auth-guard/auth-guard.service";
import {SignupComponent} from "./components/signup/signup.component";
import {SummaryComponent} from "./components/summary/summary.component";

const routes: Routes = [
  { path: '', component: SummaryComponent},
  { path: 'init-setup', component: InitialSetupComponent, canActivate: [AuthGuardService]},
  { path: loginPath, component: LoginComponent},
  { path: signUpPath, component: SignupComponent},
  { path: '**', component: SummaryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
