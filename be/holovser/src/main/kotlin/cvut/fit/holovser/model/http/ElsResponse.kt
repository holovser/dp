package cvut.fit.holovser.model.http

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
class ElsDocumentResponse(var id: String, var source: Any? = null) {
}