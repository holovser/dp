import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Store} from "@ngrx/store";
import {AuthInterface, ElsDbInterface, State} from "../../state/reducers";
import {Router} from "@angular/router";
import {authenticateUser, createNeo4jDb, updateAuthError, updateElsConfig} from "../../state/actions";
import {catchError, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(private http: HttpClient,
              private store: Store<{ dbConfigState: State }>,
              private router: Router) {
    this.store.subscribe((store: { dbConfigState: State }) => {
      this.auth = store.dbConfigState.auth;
    });
  }

  private auth: AuthInterface | undefined;
  private timeoutId?: number | undefined;

  public isAuthenticated(): boolean {
    return this.auth != undefined && this.auth.username != undefined &&
      this.auth.accessToken != undefined && this.auth.refreshToken != undefined
  }


  public login(username: string, password: string) {
    this.http.post(
      environment.serverUrl + "/session",
      {username: username, password: password},
      {
        headers: {
          accessToken: (this.auth?.accessToken == undefined) ? "" : this.auth.accessToken
        }
      }).pipe(
      catchError((error) => {
        this.store.dispatch(updateAuthError({authError: 'Credentials are not valid. Please try once more'}))
        // this.router.navigate(['/']);
        return throwError(error.message);
      })
    )
      .subscribe(
      (result: any) => {
        this.dispatchUserAuth(result);
        this.store.dispatch(updateAuthError({authError: undefined}))
        this.router.navigate(['/']);
        this.refreshAuthentication(
          result.auth.tokenTimeToLive,
          result.auth.accessToken,
          result.auth.refreshToken
        );
      }
    );
  }


  private refreshAuthentication(tokenTimeToLive: number, accessToken: string, refreshToken: string) {

    if (this.auth == undefined) {
      return;
    }
    const now = new Date()
    let timeout = tokenTimeToLive*1000 - now.getTime() - 400_000

    this.timeoutId = setTimeout(() => {
      if (this.auth != undefined) {
        this.http.put(
          environment.serverUrl + "/token",
          null,
          {
            headers: {
              accessToken: accessToken,
              refreshToken: refreshToken
            }
          }
        ).pipe(
          catchError((error) => {
            if (this.auth != undefined) {
              this.router.navigate(["/login"]);
            }
            return throwError(error.message);
          })
        ).subscribe((result: any) => {
          this.dispatchUserAuth(result);
          this.refreshAuthentication(
            (result.auth.tokenTimeToLive),
            result.auth.accessToken,
            result.auth.refreshToken
          );
        });
      }
    }, timeout)
  }

  public signup(username: string, password: string) {
    this.http.post(
      environment.serverUrl + "/user",
      {username: username, password: password},
      {
        headers: {
          accessToken: (this.auth?.accessToken == undefined) ? "" : this.auth.accessToken
        }
      }).pipe(
      catchError((error) => {
        if (error.status === 409) {
          this.store.dispatch(updateAuthError(
            { authError: 'This username is already used. Try another one' }
            )
          )
        }
        return throwError(error.message);
      })
    ).subscribe((result: any) => {
      this.dispatchUserAuth(result);
      this.store.dispatch(updateAuthError({authError: undefined}));
      this.router.navigate(['init-setup']);
      this.refreshAuthentication(
        result.auth.tokenTimeToLive,
        result.auth.accessToken,
        result.auth.refreshToken
      );
    })
  }


  private dispatchUserAuth(result: any) {

    let schema: any = JSON.parse(result.els.schema);
    if (!schema) {
      schema = {};
    }

    this.store.dispatch(authenticateUser({
      user: {
        auth: result.auth,
        neo4j: result.neo4j,
        els: {
          schema: schema,
          ...result.els
        }
      }
    }));
  }

  public createElasticIndex(elsSchema: object, els?: ElsDbInterface) {
    this.http.post(
      environment.serverUrl + "/index",
      {elsSchema: elsSchema}
    ).subscribe((result: any) => {
      this.store.dispatch(updateElsConfig({
        els: {
          ...(els!!),
          schema: elsSchema,
          index: result.els.index,
          accessToken: result.els.accessToken
        }
      }));
    });
  }

  public createNeo4jDb() {
    this.http.post(
      environment.serverUrl + "/graph",
      null
    ).subscribe((result: any) => {
      // const baseUrl = environment.serverUrl + `/${this.auth?.username}/graph`;
      // this.store.dispatch(updateNeo4jAccessToken(result.auth.accessToken));
      result.neo4j.accessToken;
      this.store.dispatch(
        createNeo4jDb(
          { neo4j: { accessToken: result.neo4j.accessToken}}
        )
      );
    });
  }

  public fetchElsConfig() {
    this.http.get(
      environment.serverUrl + "/index"
    ).subscribe((result: any) => {
      this.store.dispatch(updateElsConfig({
        els: {
          schema: JSON.parse(result.schema),
          index: result.index,
          accessToken: result.accessToken,
          schemaLocked: result.schemaLocked
        }
      }));
    });
  }

  public stopRefreshing() {
    clearTimeout(this.timeoutId);
  }
}
