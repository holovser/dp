package cvut.fit.holovser.util

import java.util.regex.Matcher
import java.util.regex.Pattern


/**
 * Function which removes 'owner' information and adds correct 'owner' info
 *
 * @param query String
 * @param owner String
 * @return String
 */
fun matchQueryTransform(query: String, owner: String): String {

    val transformedQueryLines: ArrayList<String> = ArrayList()
    val queryLines: List<String> = query.split('\n');
//    val newLinedQuery = query.replace(';', '\n');

    for (line in queryLines) {
        val trimmedLine = line.trim()
        if (trimmedLine.startsWith("match", ignoreCase = true)) {
            val elementWithoutOwner: String = removeOwnerProperty(line)
            val transformedQuery: String = addAuthenticOwner(elementWithoutOwner, owner = owner)
            transformedQueryLines.add(transformedQuery)
        } else {
            transformedQueryLines.add(trimmedLine)
        }
    }

    return transformedQueryLines.joinToString(separator = "\n")
}


/**
 * Removes owner properties from a query
 *
 * @param queryElement String
 * @return String
 */
fun removeOwnerProperty(queryElement: String): String {
    return queryElement
        .replace("owner[ \\t]*:[ \\t]*((\"[\\w \\t]*\")|('[\\w \\t]*')),?[ \\t]*".toRegex(), "")
}


/**
 * Adds owner information to query nodes, relationships
 *
 * @param query String
 * @param owner String
 * @return String
 */
fun addAuthenticOwner(query: String, owner: String, ignoreNodes: ArrayList<String> = ArrayList()): String {
//    val containsPropertySection: Boolean = query.matches(".*\\{.*".toRegex());
    var transformedQuery: String = ""

    var splitQuery = query.split(",")
    var negativeExpr = ""
//
    for (ignore in ignoreNodes) {
        negativeExpr += "(?!$ignore)"
    }

    for (subQuery in splitQuery) {
        val transformedSubQuery = subQuery.replace("}".toRegex(), ", owner: '$owner' }")
//            .replace("}\\s*([)\\]])".toRegex(), ", owner: '$owner' }$1") //  } -> , owner: 'exampleOwner' }
            .replace("\\((?!.*\\{.*})$negativeExpr([^-]*)\\)".toRegex(), "($1{ owner: '$owner' })") //  (n:Person) -> (n:Person: { owner: 'exampleOwner' }) BUT does not match elements containing {}

//            .replace("}".toRegex(), ", owner: $owner ]")
            .replace("\\[(?!.*\\{.*})$negativeExpr([^-]*)]".toRegex(), "[$1{ owner: '$owner' }]") // [rel:ACTED_IN] -> [rel:ACTED_IN: { owner: 'exampleOwner' }] BUT does not match elements containing {}
//            .replace("\\[([^-]*)]".toRegex(), "[$1{ owner: '$owner' }]")

        transformedQuery += transformedSubQuery
    }
    return transformedQuery
}

/**
 * Removes lines which contains 'set' or 'create' command
 *
 * @param query String
 * @return String
 */
fun escapeSetCreateCommands(query: String): String {
    val queryLines: List<String> = query.split('\n')
    var resultQueryLines = ArrayList<String>()

    queryLines.forEach {

        val trimmedLine = it.trim()
        if (!trimmedLine.startsWith("set", ignoreCase=true)
            && !trimmedLine.startsWith("create", ignoreCase=true)) {
            resultQueryLines.add(it)
        }
    }

    return resultQueryLines.joinToString(separator = "\n")
}


/**
 * Removes lines if a line contains 'set' and 'owner' keywords at the same time
 *
 * @param query String
 * @return String
 */
fun escapeDangerousSetCommand(query: String): String {
    val queryLines: List<String> = query.split('\n');

    val secureLines = queryLines.map {
        val trimmedLine = it.trim()
        if (trimmedLine.startsWith("set", ignoreCase = true) &&
            trimmedLine.contains("owner", ignoreCase = true)
        ) {
            ""
        } else {
            it
        }
    }

    return secureLines.joinToString(separator = "\n")
}


/**
 * Function which removes all 'create' commands from a query
 *
 * @param query String
 * @return String
 */
fun escapeDangerousCreateCommand(query: String): String {
    val queryLines: List<String> = query.split('\n');

    val secureLines = queryLines.map {
        val trimmedLine = it.trim()
        if (trimmedLine.startsWith("create", ignoreCase = true) &&
            trimmedLine.contains("owner", ignoreCase = true)
        ) {
            ""
        } else {
            it
        }
    }

    return secureLines.joinToString(separator = "\n")
}


/**
 * Function which removes 'owner' information and adds correct 'owner' info
 *
 * @param query String
 * @param owner String
 * @return String
 */
fun createQueryTransform(query: String, owner: String): String {
    val transformedQueryLines: ArrayList<String> = ArrayList()
    val queryLines: List<String> = query.split('\n');
    var declaredProperties: ArrayList<String> = ArrayList()

//    val newLinedQuery = query.replace(';', '\n');

    for (line in queryLines) {
        val trimmedLine = line.trim()
        if (trimmedLine.startsWith("create", ignoreCase = true)) {
            val elementWithoutOwner: String = removeOwnerProperty(line)
            val transformedQuery: String =
                addAuthenticOwner(elementWithoutOwner, owner = owner, ignoreNodes = declaredProperties)
            transformedQueryLines.add(transformedQuery)
        } else if (trimmedLine.startsWith("match", ignoreCase = true)) {
            val pattern = Pattern.compile("\\(([a-zA-Z]*).*?\\)")
            val matcher = pattern.matcher(trimmedLine)
            while (matcher.find()) {
                declaredProperties.add(matcher.group(1));
            }
            transformedQueryLines.add(addAuthenticOwner(trimmedLine, owner = owner))
        } else {
            transformedQueryLines.add(trimmedLine)
        }
    }

    return transformedQueryLines.joinToString(separator = "\n")
}

