import { Component, OnInit } from '@angular/core';
import {AuthInterface, Neo4jDbInterface, State} from "../../state/reducers";
import {Store} from "@ngrx/store";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-neo4j-summary',
  templateUrl: './neo4j-summary.component.html',
  styleUrls: ['./neo4j-summary.component.scss']
})
export class Neo4jSummaryComponent implements OnInit {

  constructor(public store: Store<{dbConfigState: State}>) { }

  neo4jDb?: Neo4jDbInterface = undefined;
  auth?: AuthInterface = undefined;
  baseUrl = environment.serverUrl + `/api/neo4j`;

  ngOnInit(): void {
    this.store.subscribe( (store: {dbConfigState: State}) => {
      this.auth = store.dbConfigState.auth
      this.neo4jDb = store.dbConfigState.neo4j
    });
  }

}
