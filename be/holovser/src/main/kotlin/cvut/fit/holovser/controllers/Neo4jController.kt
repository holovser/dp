package cvut.fit.holovser.controllers

import cvut.fit.holovser.model.els.User
import cvut.fit.holovser.services.AuthService
import cvut.fit.holovser.services.Neo4jService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController


@RestController
class Neo4jController(var authService: AuthService,
                      var neo4jService: Neo4jService
) {

    @PostMapping("/graph")
    fun createGraph(@RequestHeader accessToken: String
    ): ResponseEntity<User> {
        val user: User = authService.getUserByAccessToken(accessToken)

        try {
            neo4jService.createGraph(user)
        } catch (ex: Throwable) {
            println(ex.message)
            return ResponseEntity.status(500).build()
        }

        return ResponseEntity.status(202).body(user)
    }
}