import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {HttpService} from "../../services/auth/http.service";
import {AuthInterface, State} from "../../state/reducers";
import {Store} from "@ngrx/store";
import {updateAuthError} from "../../state/actions";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  constructor(private route:ActivatedRoute,
              private httpService: HttpService,
              private store: Store<{ dbConfigState: State }>) {
  }

  public profileForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  auth?: AuthInterface = undefined;

  ngOnInit(): void {
    this.store.subscribe((store: { dbConfigState: State }) => {
      this.auth = store.dbConfigState.auth;
    });
  }

  ngOnDestroy(): void {
    this.store.dispatch(updateAuthError({authError: undefined}));
  }

  public onSubmit() {
    this.login(this.profileForm.value['username'], this.profileForm.value['password']);
  }

  private login(username: string, password: string) {
    this.httpService.login(username, password);
  }

}
