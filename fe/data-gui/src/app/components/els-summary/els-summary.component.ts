import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import { environment } from 'src/environments/environment';
import {ElsDbInterface, State} from "../../state/reducers";

@Component({
  selector: 'app-els-summary',
  templateUrl: './els-summary.component.html',
  styleUrls: ['./els-summary.component.scss']
})
export class ElsSummaryComponent implements OnInit {

  constructor(public store: Store<{dbConfigState: State}>) { }

  els?: ElsDbInterface;
  baseUrl: string = environment.serverUrl

  ngOnInit(): void {
    this.store.subscribe( (store: {dbConfigState: State}) => {
      this.els = store.dbConfigState.els
    });
  }

  public serializeSchemaToHtml(schema: any): string {
    let res: string = '';

    let key: keyof typeof schema;
    for (key in schema) {
      const item = schema[key];

      if (Array.isArray(item)) {
        res += `<div><br> ${key}: [ <br>`
          + this.serializeSchemaToHtml(item[0])
          + ' <br> ] </div>'
      } else if ( typeof item === 'object' && !Array.isArray(item)) {
        if (Object.keys(item).length == 0) {
          res += `<div> <br> ${key}: { <br> `
            + '<span>object</span>'
            + '<br> } </div>'
        } else {
          res += `<div> <br>  ${key}: { <br> `
            + this.serializeSchemaToHtml(item)
            + '<br> } </div>'
        }
      } else {
        res += `<br> <span> ${key}: ${item} </span> <br>`;
      }
    }

    return res;
  }

}
